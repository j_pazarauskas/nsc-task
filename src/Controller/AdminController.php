<?php

namespace App\Controller;

use App\Repository\MessagesRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * Display a listing of the guest messages
     * @Route("/", name="admin_index")
     */
    public function index(MessagesRepository $messagesRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $itemPerPage = 10;
        $messagesQuery = $messagesRepository->findAll(); // data query

        $messages = $paginator->paginate(
            $messagesQuery, $request->query->getInt('page', 1), $itemPerPage);

        return $this->render('admin/index.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * Show user login form
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('admin_index');
        }

//         get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
//         last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('admin/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * User logout page
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
