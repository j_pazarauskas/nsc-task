<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Form\MessagesType;
use App\Repository\MessagesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Cookie;

class MessagesController extends AbstractController
{
    /**
     * Display a listing of the messages
     * @Route("/", name="messages_index", methods={"GET"})
     */
    public function index(MessagesRepository $messagesRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $itemsPerPage = 10; // items per page
        $messagesQuery = $messagesRepository->findAllExceptId(); // data query

// the query is executed with paginator and the data is received
        $messages = $paginator->paginate(
            $messagesQuery, $request->query->getInt('page', 1), $itemsPerPage);

        $userNameCookie = $request->cookies->get('user_name'); // guest name from cookie

        $totalUserMessages = $messagesRepository->getTotalMessage($userNameCookie); // number of guest messages
        $lastVisitTime = date('Y-m-d h:i', $request->cookies->get('last_visit_time')); // last visit time from cookie

        setcookie("last_visit_time", strtotime('now'), time() + (3600 * 24)); // new value of last visit time

        return $this->render('messages/index.html.twig', [
            'messages' => $messages,
            'userName' => $userNameCookie,
            'totalUserMessages' => $totalUserMessages[1],
            'lastVisitTime' => $lastVisitTime
        ]);
    }

    /**
     * Display a message form or store new message in storage
     * @Route("/new", name="messages_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $message = new Messages();
        $form = $this->createForm(MessagesType::class, $message, [
            // Time protection
            'antispam_time' => true,
            'antispam_time_min' => 3,
            'antispam_time_max' => 60,

            // Honeypot protection
            'antispam_honeypot' => true,
            'antispam_honeypot_class' => 'hidden',
            'antispam_honeypot_field' => 'email',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->get('email') == null) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            setcookie("user_name", $message->getGuest(), time() + (3600 * 24)); // set user name cookie

// logged in users redirecting to admin_index page
            if ($this->getUser() !== null)
                return $this->redirectToRoute('admin_index');

// else to messages_index
            return $this->redirectToRoute('messages_index');
        }

        return $this->render('messages/new.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Store data or show the form for editing the specified message
     * This method can only be used by an admin
     * @Route("/admin/{id}/edit", name="messages_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Messages $message): Response
    {
        $form = $this->createForm(MessagesType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_index');
        }

        return $this->render('messages/edit.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Remove the specified message from storage
     * This method can only be used by an admin
     * @Route("/admin/{id}", name="messages_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Messages $message): Response
    {
        if ($this->isCsrfTokenValid('delete' . $message->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($message);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_index');
    }
}
