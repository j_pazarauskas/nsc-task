<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Messages;

class MessagesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 25; $i ++) {
            $messages = new Messages();
            $messages->setGuest("Some guest name " . rand(1, 10));
            $messages->setMessage("Text " . $i);
            $manager->persist($messages);
        }

        $manager->flush();
    }
}
