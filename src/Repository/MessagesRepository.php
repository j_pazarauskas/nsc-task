<?php

namespace App\Repository;

use App\Entity\Messages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }

    /**
     * @return Messages[] Returns an array of Messages objects
     */
    public function findAllExceptId()
    {
        return $this->createQueryBuilder('m')->select(['m.guest', 'm.message'])
            ->orderBy('m.id', 'ASC')
            ->getQuery();;
    }

    public function findAll()
    {
        return $this->createQueryBuilder('m')->orderBy('m.id', 'ASC')->getQuery();
    }

    public function getTotalMessage($user)
    {
        return $this->createQueryBuilder('m')->select('count(m.id)')
            ->where('m.guest = :user')->setParameter('user', $user)
            ->getQuery()->getSingleResult();
    }

}
