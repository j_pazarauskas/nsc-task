## Here is my completed task.

Follow the instructions to install the project:

1.  Run "composer install" command to download the required packages;
2.  Open the .env file and customize this line: DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7";
3.  Run "php bin/console doctrine:database:create" to create database;
4.  Run "php bin/console doctrine:migrations:migrate" to make tables migration;
5.  Use the following command to quickly add fake data: php bin/console doctrine:fixtures:load
6.  And finally run the symfony server command (you definitely have symfony): symfony serve.


## About web project

Public part:

  The index page "/" displays the paginated list of the guest's names and their messages.
  If the user creates a new message, then his name is stored in cookies and displayed on the index page.
  This index page also displays the guest total number of messages and the time of the last visit.


## Admin part:

  Unlike public pages, all urls starting with "/admin" are protected by authorization except the "admin/login" page.
  You have one admin user in your database, his login credentials are demoUser, demoPass.
  The admin user has additional permissions to edit and delete guest messages.

## Additional information:
  A "knplabs/knp-paginator-bundle" package is used for messages pagination.
  The new message form "/new" is spam protected using the "nucleos/antispam-bundle" package.